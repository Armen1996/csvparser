<?php

namespace App\Repositories\Products;

use App\Dto\GetProductsDto;
use Illuminate\Pagination\LengthAwarePaginator;

interface ProductsRepositoryInterface
{
    public function insert(array $products): bool;
    public function list(GetProductsDto $dto): LengthAwarePaginator;
}
