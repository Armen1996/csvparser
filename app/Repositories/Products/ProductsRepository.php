<?php

namespace App\Repositories\Products;

use App\Dto\GetProductsDto;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductsRepository implements ProductsRepositoryInterface
{
    private function query(): Builder
    {
        return Product::query();
    }

    public function insert(array $products): bool
    {
        $this->query()->insert($products);

        return true;
    }

    public function list(GetProductsDto $dto): LengthAwarePaginator
    {
        $query = $this->query();

        if (!is_null($dto->q)) {
            $query->where('name', 'like', "%$dto->q%");
        }

        return $query->paginate(
            $dto->perPage,
            ['*'],
            'page',
            $dto->page
        );
    }
}
