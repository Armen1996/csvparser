<?php

namespace App\Jobs;

use App\Repositories\Products\ProductsRepositoryInterface;
use App\Transformers\ProductsCollectionToArrayTransformer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SaveProductsByGroupJob implements ShouldQueue
{
    public Collection $groupProducts;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $groupProducts)
    {
        $this->groupProducts = $groupProducts;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProductsRepositoryInterface $productsRepository): void
    {
        $products = ProductsCollectionToArrayTransformer::run($this->groupProducts);

        $productsRepository->insert($products);
    }
}
