<?php

namespace App\Dto;

use App\Http\Requests\GetProductsRequest;
use Spatie\DataTransferObject\DataTransferObject;

class GetProductsDto extends DataTransferObject
{
    public string $page;
    public string $perPage;
    public ?string $q;

    public static function fromRequest(GetProductsRequest $request): self
    {
        return new self(
            page: $request->getPage(),
            perPage: $request->getPerPage(),
            q: $request->getQ(),
        );
    }
}
