<?php

namespace App\Transformers;

use Illuminate\Support\Collection;

class ProductsCollectionToArrayTransformer
{
    public static function run(Collection $groupProducts): array
    {
        $products = [];
        foreach ($groupProducts as $product) {
            $product = array_values(explode(';', $product));
            $products[] = [
                'code' => $product[0],
                'name' => $product[1],
                'level1' => $product[2] ?? '',
                'level2' => $product[3] ?? '',
                'level3' => $product[4] ?? '',
                'price' => $product[5] ?? 0,
                'priceCP' => $product[6] ?? 0,
                'count' => $product[7] ?? 0,
                'properties' => $product[8] ?? '',
                'purchases' => $product[9] ?? 0,
                'unit' => $product[10] ?? '',
                'image' => $product[11] ?? '',
                'title' => $product[12] ?? '',
                'description' => $product[13] ?? '',
            ];
        }

        return $products;
    }
}
