<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductsFromCsvRequest extends FormRequest
{
    const FILE = 'file';

    public function rules(): array
    {
        return [
            self::FILE => [
                'required',
                'file',
            ],
        ];
    }

    public function getFileContent(): string
    {
        return $this->file('file')->get();
    }
}
