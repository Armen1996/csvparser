<?php

namespace App\Http\Controllers;

use App\Actions\CreateProductsFromCsvAction;
use App\Actions\GetProductsAction;
use App\Dto\GetProductsDto;
use App\Http\Requests\CreateProductsFromCsvRequest;
use App\Http\Requests\GetProductsRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    private CreateProductsFromCsvAction $createProductsFromCsvAction;
    private GetProductsAction $getProductsAction;

    public function __construct(
        CreateProductsFromCsvAction $createProductsFromCsvAction,
        GetProductsAction $getProductsAction
    ) {
        $this->createProductsFromCsvAction = $createProductsFromCsvAction;
        $this->getProductsAction = $getProductsAction;
    }

    public function createFromCsv(CreateProductsFromCsvRequest $request): JsonResponse
    {
        $file = $request->getFileContent();

        $this->createProductsFromCsvAction->createFromCsv($file);

        return response()->json(['success' => true], Response::HTTP_CREATED);
    }

    public function list(GetProductsRequest $request): AnonymousResourceCollection
    {
        $listDto = GetProductsDto::fromRequest($request);

        return $this->getProductsAction->list($listDto);
    }
}
