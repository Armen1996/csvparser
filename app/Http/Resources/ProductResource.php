<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'code' => $this->resource->code,
            'name' => $this->resource->name,
            'level1' => $this->resource->level1,
            //TODO need to add all columns
        ];
    }
}
