<?php

namespace App\Actions;

use App\Jobs\SaveProductsByGroupJob;

class CreateProductsFromCsvAction
{
    public function createFromCsv(string $file): bool
    {
        $items = collect(explode('\n', $file));

        $groupProducts = $items->chunk(50);

        foreach ($groupProducts as $group) {
            SaveProductsByGroupJob::dispatch($group);
        }

        return true;
    }
}
