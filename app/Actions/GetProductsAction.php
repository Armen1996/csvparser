<?php

namespace App\Actions;

use App\Dto\GetProductsDto;
use App\Http\Resources\ProductResource;
use App\Repositories\Products\ProductsRepositoryInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GetProductsAction
{
    private ProductsRepositoryInterface $productsRepository;

    public function __construct(ProductsRepositoryInterface $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    public function list(GetProductsDto $dto): AnonymousResourceCollection
    {
        $products = $this->productsRepository->list($dto);

        return ProductResource::collection($products);
    }
}
