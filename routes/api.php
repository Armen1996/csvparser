<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('product')->group(function (Router $route) {
    $route->post('/create-from-csv', [\App\Http\Controllers\ProductController::class, 'createFromCsv']);
    $route->get('/', [\App\Http\Controllers\ProductController::class, 'list']);
});
